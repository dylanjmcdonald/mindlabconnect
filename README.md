## COMPILING SASS
From the command line, navigate to the trunk root and run the following command:
```sh
$ sass --watch MindLabConnect/Content/sass:MindLabConnect/Content/css
```