﻿$(document).ready(function () {

    var $window = $(window);

    // Parallaxing Script
    $('[data-type="background"]').each(function () {
        var $bgobj = $(this);
        $(window).scroll(function () {
            var yPos = -($window.scrollTop() / $bgobj.data('speed'));
            var coords = '50% ' + yPos + 'px';
            $bgobj.css({ backgroundPosition: coords });
        });
    });

    // Menu Trigger Script (1)
    var dlgtrigger = document.querySelectorAll('[data-dialog]');
    for (var i = 0; i < dlgtrigger.length; i++) {
        var menu = document.getElementById(dlgtrigger[i].getAttribute('data-dialog'));
        var dlg = new DialogFx(menu);
        dlgtrigger[i].addEventListener('click', dlg.toggle.bind(dlg));
    }

    // Menu Trigger Script (2)
    $('.ml-menu-button').on('click', function (e) {
        e.preventDefault();
        $(this).toggleClass('ml-menu-button-active');
    });
});