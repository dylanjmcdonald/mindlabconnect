﻿using System.Web.Optimization;

namespace MindLabConnect
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js",
                      "~/Scripts/classie.js",
                      "~/Scripts/dialogFx.js",
                      "~/Scripts/main.js"));

            bundles.Add(new StyleBundle("~/bundles/css").Include(
                      "~/Content/fonts/mpix234/stylesheet.css",
                      "~/Content/css/dialog.css"));
        }
    }
}
