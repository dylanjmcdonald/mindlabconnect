﻿using System.Web.Mvc;

namespace MindLabConnect.Controllers
{
    public class LandingController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}