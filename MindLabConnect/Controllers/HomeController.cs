﻿using System.Web.Mvc;

namespace MindLabConnect.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}